# 3D-HASE

3D-HASE is an abbreviation for **3** **D**imensional - **H**aptic **A**udio **S**patialization **E**nvironment. In simple terms it is an asset for the game development environment Unity that inlcudes the Phantom OMNI haptic device for the composition of aural 3D pieces. 

At this point it is not capable to form an entire composition from a mixture of trajectories of sound sources, but it allows users to experience the workflow with the device in the process of creation and modification of trajectories. In the background of the working process a set of OSC commands in the [SpatDIF format](http://www.spatdif.org/specifications.html) are sent in order to be able to render the sounding result of the sound source travelling the trajectory. 

3D-HASE is currently under development and yet just provides a proof-of-concept software application. 