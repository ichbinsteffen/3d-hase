﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpOSC;
using Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript;

//namespace Assets.Haptic_Project_Components.Scripts.Plugin_Import
namespace OSCInterface
{
    class OSCInterface : MonoBehaviour
    {
        static int stdSendPort = 1337;
        static int stdRecValuesPort = 7980; // the port where return values come to
        static int stdRecStatusPort = 7072;

        private TrajectoryEditorScript TrajectoryEditor;
        private int numTrajectories;

        private List<GameObject> Trajectories;

        public static UDPSender mySender;
        public static UDPListener myListener;

        static private OscMessage numSources;
        static private OscMessage numSpeakers;
        static private OscMessage masterVolume;
        static private OscMessage spatMethod;
        static private OscMessage OnOff;
        static private OscMessage sampleRate;
        private static OscMessage clientStatus;

        void Start()
        {
            Trajectories = TrajectoryEditorScript.Trajectory_List;
            numTrajectories = TrajectoryEditorScript.trajCount;

            mySender = new SharpOSC.UDPSender("127.0.0.1", stdSendPort);

            mySender.Send(new SharpOSC.OscMessage("/spatdif/info/author", "Steffen Ebner"));
            mySender.Send(new SharpOSC.OscMessage("/spatdif/info/client", "3D-HASE"));
            mySender.Send(new SharpOSC.OscMessage("/spatdif/info/client-status", "true"));
            mySender.Send(new SharpOSC.OscMessage("/spatdif/settings/position-unit", "openGL"));
        }

        void Update()
        {
            
        }

        // Seems to be unused! 
        public static void dispatch_OSC_MSG_LIST(List<OscMessage> list)
        {
            for (int i = 0; i <= list.Count; i++)
            {
                mySender.Send(list.ElementAt(i));
            }
        }

        public int[] IP { get; private set; }

        // Adding a Trajectory
        public static void add_Source(int id)
        {
            mySender.Send(new SharpOSC.OscMessage("/spatdif/scene/add-source", id));
        }

        public static void set_Volume(int id, float vol)
        {
            mySender.Send(new SharpOSC.OscMessage("/spatdif/source/"+id.ToString()+"/volume", vol));
        }

        public static void set_Loop(int id, bool loop)
        {
            mySender.Send(new SharpOSC.OscMessage("/source/"+id.ToString()+"/loop", loop));
        }

        public static void set_Mute(int id, bool mute)
        {
            mySender.Send(new SharpOSC.OscMessage("/spatdif/source/"+id.ToString()+"/present", mute));
        }

        public static void set_AudioFilePath(int id, string file)
        {
            // first convert windows style path to unix friendly path
            string s = file.Replace(@"\", @"/");
            Debug.Log(s);
            mySender.Send(new SharpOSC.OscMessage("/spatdif/source/"+id.ToString()+"/media", s));
        }

        public static void moveTestSynth(float x, float y, float z)
        {
            mySender.Send(new SharpOSC.OscMessage("/testSynth", "0", x, y, z));
        }

        public static void createTestSynth(string name)
        {
            mySender.Send(new SharpOSC.OscMessage("/testSynth", name));
        }

        public static void set_SourcePosition(int id, float x, float y, float z)
        {
            /* 
            When this function is called (from the TrajectoryScript) it is sent with z = z*-1
            so the OSC message is truly sent with OpenGL coordinates.
             */

            //mySender.Send(new SharpOSC.OscMessage("/source/positionSpherical", id, rho, theta, phi));
            mySender.Send(new SharpOSC.OscMessage("/spatdif/source/"+id.ToString()+"/position", x, y, z));
        }

        public static void start_playing(int id, float startSec, bool loop)
        {
            mySender.Send(new SharpOSC.OscMessage("/spatdif/source/"+id.ToString()+"/play", startSec, loop));
        }

        public static void pause_playing(int id)
        {
            mySender.Send(new SharpOSC.OscMessage("/spatdif/"+id.ToString()+"/source/pause", id));
        }

        public static void set_positionUnit(string coordinatesUnit)
        {
			// SpatDIF:
			// xyz = cartesian coordinates.	Positive X: Right; Positive Y: Front; Positive Z: Upwards.
			// aed = Spherical coordinates. Azimuth 0: Front, counter clockwise increase; Elevation 0: no incline no decline, upwards = incline.
			// openGL = Cartesian coordinates. Positive X: Right; Positive Y: Upwards; Positive Z: Front. (Really? positive z not back?)

            mySender.Send(new SharpOSC.OscMessage("/spatdif/settings/position-unit", coordinatesUnit));
        }

        public static void set_UDP_Sender(string arg_ip, int arg_port)
        {
            mySender = new SharpOSC.UDPSender(arg_ip, arg_port); ;
        }
    }
}
