﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript;
using Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript.HUDSpace;
using OSCInterface;

namespace Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript
{

    enum EditorMode
    {
        lookAndFeel = 0,
        draw = 1,
        edit = 2,
        remove = 3,
        playCue = 4
    }

    class TrajectoryEditorScript : HapticClassScript
    {

        public static List<GameObject> Trajectory_List = new List<GameObject>();
        public static EditorMode editorMode;
        public static int trajCount;

        //Generic Haptic Functions
        public GenericFunctionsClass myGenericFunctionsClassScript;
        private theGUI myHUDScript;
        float[] workspaceUpdateValue = new float[1];

        public bool workspaceNeedsHapticRefresh = false;

        void Awake()
        {

            myGenericFunctionsClassScript = transform.GetComponent<GenericFunctionsClass>();
            myHUDScript = GameObject.Find("Canvas").GetComponent<theGUI>();

            Trajectory_List = new List<GameObject>();
        }

        void Start()
        {

            //myTrajectoryScript = GetComponent<TrajectoryScript>();
            myGenericFunctionsClassScript = transform.GetComponent<GenericFunctionsClass>();

            if (PluginImport.InitHapticDevice())
            {
                Debug.Log("OpenGL Context Launched");
                Debug.Log("Haptic Device Launched");

                myGenericFunctionsClassScript.SetHapticWorkSpace();
                myGenericFunctionsClassScript.GetHapticWorkSpace();

                //Update Workspace as function of camera
                //PluginImport.UpdateWorkspace(myHapticCamera.transform.rotation.eulerAngles.y);  //To be deprecated

                //Update the Workspace as function of camera
                for (int i = 0; i < workspaceUpdateValue.Length; i++)
                    workspaceUpdateValue[i] = myHapticCamera.transform.rotation.eulerAngles.y;

                PluginImport.UpdateHapticWorkspace(ConverterClass.ConvertFloatArrayToIntPtr(workspaceUpdateValue));

                //Set Mode of Interaction
                /*
                 * Mode = 0 Contact
                 * Mode = 1 Manipulation - So objects will have a mass when handling them
                 * Mode = 2 Custom Effect - So the haptic device simulate vibration and tangential forces as power tools
                 * Mode = 3 Puncture - So the haptic device is a needle that puncture inside a geometry
                 */
                PluginImport.SetMode(ModeIndex);
                //Show a text descrition of the mode
                myGenericFunctionsClassScript.IndicateMode();

                //Set the touchable face(s)
                PluginImport.SetTouchableFace(ConverterClass.ConvertStringToByteToIntPtr(TouchableFace));

            }
            else
                Debug.Log("Haptic Device cannot be launched");

            // Viscous Force Example
            myGenericFunctionsClassScript.SetEnvironmentViscosity();

            // Constant Force Example - We use this environmental force effect to simulate the weight of the cursor
            myGenericFunctionsClassScript.SetEnvironmentConstantForce();

            // Friction Force Example
            //myGenericFunctionsClassScript.SetEnvironmentFriction();

            // Spring Force Example
            //myGenericFunctionsClassScript.SetEnvironmentSpring();


            myGenericFunctionsClassScript.SetHapticGeometry();

            PluginImport.LaunchHapticEvent();

            editorMode = EditorMode.lookAndFeel;
            PluginImport.SetMode(1);

        }


        void Update()
        {

            Vector3 cursorPos = myGenericFunctionsClassScript.myHapticClassScript.hapticCursor.transform.position;
            Quaternion cursorQuat = myGenericFunctionsClassScript.myHapticClassScript.hapticCursor.transform.localRotation;
            trajCount = Trajectory_List.Count;

            //Update the Workspace as function of camera
            for (int i = 0; i < workspaceUpdateValue.Length; i++)
                workspaceUpdateValue[i] = myHapticCamera.transform.rotation.eulerAngles.y;

            PluginImport.UpdateHapticWorkspace(ConverterClass.ConvertFloatArrayToIntPtr(workspaceUpdateValue));

            myGenericFunctionsClassScript.UpdateGraphicalWorkspace();

            PluginImport.RenderHaptic();

            //Associate the cursor object with the haptic proxy value  
            myGenericFunctionsClassScript.GetProxyValues();
            myGenericFunctionsClassScript.GetTouchedObject();

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            //------ Keystrokes 

            const float rotSpeed = 1.0f;

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                myHapticCamera.transform.RotateAround(Vector3.zero, new Vector3(0.0f, 1.0f, 0.0f), rotSpeed);
            }
            if (Input.GetKey(KeyCode.RightArrow))
            {
                myHapticCamera.transform.RotateAround(Vector3.zero, new Vector3(0.0f, 1.0f, 0.0f), -rotSpeed);
            }

            if (Input.GetKey(KeyCode.UpArrow))
            {
                myHapticCamera.transform.RotateAround(Vector3.zero, new Vector3(1.0f, 0.0f, 0.0f), rotSpeed);
            }
            if (Input.GetKey(KeyCode.DownArrow))
            {
                myHapticCamera.transform.RotateAround(Vector3.zero, new Vector3(1.0f, 0.0f, 0.0f), -rotSpeed);
            }

            if (Input.GetKey(KeyCode.PageUp))
            {
                myHapticCamera.GetComponent<Camera>().fieldOfView -= 1;
            }
            if (Input.GetKey(KeyCode.PageDown))
            {
                myHapticCamera.GetComponent<Camera>().fieldOfView += 1;
            }

            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.F2))
            {
                editorMode = EditorMode.draw;
            }

            if (Input.GetKeyDown(KeyCode.F1))
            {
                editorMode = EditorMode.lookAndFeel;
            }

            if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.F3))
            {
                myGenericFunctionsClassScript.SetHapticGeometry();
                myGenericFunctionsClassScript.UpdateHapticObjectMatrixTransform();
                Debug.Log("Total Number of Haptic Objects: " + PluginImport.GetHapticObjectCount());
                editorMode = EditorMode.edit;
            }

            if (Input.GetKeyDown(KeyCode.K))
            {
                myGenericFunctionsClassScript.SetHapticGeometry();
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // ------ LOOK AND FEEL MODE 

            if (editorMode == EditorMode.lookAndFeel)
            {
                PluginImport.SetMode(0);
                myHUDScript.EditorModeDropdown.value = 0;

                if (PluginImport.GetButton2State())
                {
                    OSCInterface.OSCInterface.createTestSynth("test");
                }

                if (Input.GetKeyDown(KeyCode.Space) || PluginImport.GetButton1State())
                {
                    //float xNormal = x / 7.61f; // max values achievable when not rotating the camera
                    //float yNormal = y / 8.24f;
                    //float zNormal = z / 7.35f;

                    float x = cursorPos.x;
                    float y = cursorPos.y;
                    float z = cursorPos.z;

                    float xNormal = x / 10.0f;
                    float yNormal = y / 10.0f;
                    float zNormal = z / 10.0f;

                    Debug.Log("X" + (float)xNormal + " Y: " + (float)yNormal + " Z: " + (float)zNormal);
                }
            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // ------ EDIT MODE 

            if (editorMode == EditorMode.edit)
            {
                PluginImport.SetMode(1);
                myHUDScript.EditorModeDropdown.value = 2;

                if (Input.GetKeyDown(KeyCode.O))
                {
                    Trajectory_List.ElementAt(trajCount - 1).GetComponent<TrajectoryScript>().update_RawData_from_TrajPoints();
                }

                if (Input.GetKeyDown(KeyCode.L))
                    Trajectory_List.ElementAt(trajCount - 1).GetComponent<TrajectoryScript>().update_Trajectory_Line();

            }

            //////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////////////////////////
            // ------ DRAW MODE 

            if (editorMode == EditorMode.draw)
            {
                int index = myHUDScript.singleTrajDropdown.value;
                PluginImport.SetMode(0);
                myHUDScript.EditorModeDropdown.value = 1;
                if (trajCount == 0)
                {
                    add_Trajectory();
                }
                
                if (Input.GetKeyDown(KeyCode.N))
                {
                    add_Trajectory();
                }

                if (Input.GetKeyDown(KeyCode.Space) || PluginImport.GetButton1State())
                {
                    Trajectory_List.ElementAt(index).GetComponent<TrajectoryScript>().add_TrajPointFromAssetAsChild(cursorPos, cursorQuat);
                }

                if (Input.GetKeyDown(KeyCode.B))
                {
                }

                if (Input.GetKeyDown(KeyCode.L))
                {
                    Trajectory_List.ElementAt(index).GetComponent<TrajectoryScript>().draw_TrajectoryLine_Simplified(0.1f);
                    Trajectory_List.ElementAt(index).GetComponent<TrajectoryScript>().update_DistanceData();
                }

                if (Input.GetKeyDown(KeyCode.P))
                {
                    Trajectory_List.ElementAt(index).GetComponent<TrajectoryScript>().play();
                }

                /*
                if (Input.GetKeyDown(KeyCode.K)) {
                    myGenericFunctionsClassScript.SetHapticGeometry();
                    //editorMode = EditorMode.drag;
                } 
                */

                if (Input.GetKeyDown(KeyCode.E))
                {
                    myGenericFunctionsClassScript.SetHapticGeometry();
                    Debug.Log("Total Number of Haptic Objects: " + PluginImport.GetHapticObjectCount());
                    PluginImport.SetMode(0);
                    editorMode = EditorMode.edit;
                }

                if (Input.GetKeyDown(KeyCode.Delete))
                {
                    Debug.Log(String.Format("Removed Trajectory Nr: {0}", trajCount));
                    Trajectory_List.ElementAt(index).GetComponent<TrajectoryScript>().remove_AllTrajPoints();
                    Trajectory_List.RemoveAt(index);
                }
            }
            PluginImport.UpdateHapticWorkspace(ConverterClass.ConvertFloatArrayToIntPtr(workspaceUpdateValue));
        } // End of Update()

        public static void add_Trajectory()
        {
            GameObject t = new GameObject();
            t.AddComponent<TrajectoryScript>();
            t.name = "Traj" + trajCount;
            Trajectory_List.Add(t);
            t.GetComponent<TrajectoryScript>().id = trajCount;
            OSCInterface.OSCInterface.add_Source(trajCount);
        }

        // NOT WORKING?!
        public void update_haptics()
        {
            myGenericFunctionsClassScript.SetHapticGeometry();
            //myGenericFunctionsClassScript.UpdateHapticObjectMatrixTransform();
        }

        // =====================================================================================================
        // =====================================================================================================
        // =====================================================================================================

        void OnDisable()
        {
            if (PluginImport.HapticCleanUp())
            {
                Debug.Log("Haptic Context CleanUp");
                Debug.Log("Desactivate Device");
                Debug.Log("OpenGL Context CleanUp");
            }
        }

        bool previousButtonState = false;
        string grabbedObjectName = "";

        void ActivatingGrabbedObjectPropperties()
        {
            GameObject grabbedObject;
            string myObjStringName;

            if (!previousButtonState && PluginImport.GetButtonState(1, 1))
            {

                //If the object is grabbed, the gravity is deactivated and kinematic is enabled
                //myObjStringName = ConverterClass.ConvertIntPtrToByteToString(PluginImport.GetTouchedObjectName());//GetTouchedObjectName() - To be deprecated
                myObjStringName = ConverterClass.ConvertIntPtrToByteToString(PluginImport.GetTouchedObjName(1));
                Debug.Log("Grabbed Object: " + myObjStringName);

                //if (!myObjStringName.Equals("null"))
                if (!myObjStringName.Equals("null"))
                {
                    Debug.Log("Grabbed Object: " + myObjStringName);
                    grabbedObject = GameObject.Find(myObjStringName);

                    //If there is a rigid body
                    if (grabbedObject.GetComponent<Rigidbody>() != null)
                    {
                        grabbedObject.GetComponent<Rigidbody>().isKinematic = true;
                        grabbedObject.GetComponent<Rigidbody>().useGravity = false;
                    }
                    grabbedObjectName = myObjStringName;
                }
                previousButtonState = true;
            }

            else if (previousButtonState && !PluginImport.GetButtonState(1, 1))
            {
                //If the object is dropped, the gravity is enabled again and kinematic is deactivated
                if (!grabbedObjectName.Equals(""))
                {
                    grabbedObject = GameObject.Find(grabbedObjectName);

                    //If there is a rigid body
                    if (grabbedObject.GetComponent<Rigidbody>() != null)
                    {
                        grabbedObject.GetComponent<Rigidbody>().isKinematic = false;
                        grabbedObject.GetComponent<Rigidbody>().useGravity = true;
                    }
                    grabbedObjectName = "";
                }
                previousButtonState = false;
            }
        }
    } // End of TrajectoryEditor
} // End of Namespace TrajectoryEditor
