﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript
{
    class SoundSourceScript : MonoBehaviour
    {
        public List<Vector3> TrajPoints;

        public double totalLineDistance;
        private List<float> eachDistance;
        public GameObject SoundSource_Sphere;
        public bool isplaying;

        private int currentDistanceID;
        private float currentStartTime;
        private float currentJourneyLength;
        private Vector3 currentStartMarker;
        private Vector3 currentEndMarker;
        public float speed = 2.0f;

        void Awake()
        {
            SoundSource_Sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            SoundSource_Sphere.gameObject.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            SoundSource_Sphere.gameObject.tag = "Touchable";
            Renderer r = SoundSource_Sphere.gameObject.GetComponent<Renderer>();
            r.material.shader = Shader.Find("Specular");
            r.material.SetColor("_Color", Color.blue);
            SoundSource_Sphere.name = "Playhead";
            SoundSource_Sphere.transform.SetParent(gameObject.transform);
            SoundSource_Sphere.transform.SetPositionAndRotation(new Vector3(0.0f, -5.0f, 0.0f), new Quaternion(0.0f, 0.0f, 0.0f, 0.0f));
            isplaying = false;
        }

        void Start()
        {
            currentDistanceID = 0;
        }

        void Update()
        {
            if (isplaying)
            {
                float distCovered = (Time.time - currentStartTime) * speed;
                float fracJourney = distCovered / currentJourneyLength;
                SoundSource_Sphere.transform.position = Vector3.Lerp(currentStartMarker, currentEndMarker, fracJourney);
                if (SoundSource_Sphere.transform.position == currentEndMarker) { nextDistance(); }
            }
            else
            { 
                TrajPoints = gameObject.GetComponent<TrajectoryScript>().Raw_Trajectory_Vectors;
                totalLineDistance = gameObject.GetComponent<TrajectoryScript>().totalLineDistance;
                eachDistance = gameObject.GetComponent<TrajectoryScript>().eachDistance;
            }
        }

        private void nextDistance()
        {
            if (currentDistanceID + 1 == TrajPoints.Count)
            {
                currentDistanceID = 0;
                isplaying = false;
            }
            else
            {
                currentStartTime = Time.time;
                currentJourneyLength = eachDistance.ElementAt(currentDistanceID);
                currentStartMarker = TrajPoints.ElementAt(currentDistanceID);
                currentEndMarker = TrajPoints.ElementAt(currentDistanceID+1);
                currentDistanceID++;
            }
        }

        public void play()
        {
            //this.followFullTrajectory();
            isplaying = true;
        }
        
        public void followFullTrajectory() 
        {
            float speed = 0.1f;

            for (int i = 0; i <= eachDistance.Count; i++)
            {
                float startTime = Time.time;
                float journeyLength = eachDistance.ElementAt(i);
                Vector3 startMarker = TrajPoints.ElementAt(i);
                Vector3 endMarker = TrajPoints.ElementAt(i+1);

                float distCovered = (Time.time - startTime) * speed;
                float fracJourney = distCovered / journeyLength;

                SoundSource_Sphere.transform.position = Vector3.Lerp(startMarker, endMarker, fracJourney);
            }
            // Code From: https://docs.unity3d.com/ScriptReference/Vector3.Lerp.html
        }


        public void placePlayhead()
        {
            /*
            Time globalPlayheadCurrentTime = 5.0;
            Time trajStart = 2;
            float trajDuration = 6;

            float localPlayheadRelativePos = (currentPlayheadTime - trajStart) / trajDuration;
            */
        }

        public void jumpTo(Vector3 v) { }
    }
}
