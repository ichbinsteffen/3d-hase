﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpOSC;

namespace Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript
{
    class TrajectoryScript : MonoBehaviour
    {
        private Vector3 STD_ORIGIN = new Vector3(0, 0, 0);
        public Vector3 Origin_Point { get; private set; }

        public float totalLineDistance;
        public List<float> eachDistance;
        public double totalDuration;

        public List<Vector3> Raw_Trajectory_Vectors = new List<Vector3>();
        public List<GameObject> Trajectory_Points = new List<GameObject>();

        public GameObject Trajectory_LineGO;
        public LineRenderer Trajectory_Line = new LineRenderer();

        public SoundSourceScript playhead;

        public float onsetTime;
        public float durationTime;

        public bool cubesAreHidden;
        public bool lineIsHidden;
        public bool lineIsLooped;
        public bool loopAudio;

        public float volume;
        public Color32 cubesColor;
        public Color32 lineColor;
        public Color32 playheadColor;

        public string associatedAudio;

        private bool invisible;

        public int id;

        void Awake()
        {
            Raw_Trajectory_Vectors = new List<Vector3>();
            Trajectory_Points = new List<GameObject>();

            Trajectory_LineGO = new GameObject();
            Trajectory_LineGO.AddComponent<LineRenderer>();
            Trajectory_LineGO.transform.SetParent(gameObject.transform);
            Trajectory_Line = Trajectory_LineGO.GetComponent<LineRenderer>();


            this.gameObject.AddComponent<SoundSourceScript>();
///////////////////////////////////////////////////////////////////////////////////////////////
// Need to create a suitable mesh for the LineRenderer in the future!
            Mesh mesh = new Mesh();
            this.gameObject.AddComponent<MeshFilter>();
            gameObject.GetComponent<MeshFilter>().mesh = mesh;
///////////////////////////////////////////////////////////////////////////////////////////////
            
			playhead = GetComponent<SoundSourceScript>();
            
            associatedAudio = "No Audio File Associated";

            cubesAreHidden = false;
            lineIsHidden = false;
            lineIsLooped = false;
            volume = 0.8f;
            onsetTime = 0.0f;
            durationTime = -0.1f;
    }

        void Start()
        {
            Vector3 vec = new Vector3(0, 0, 0);

            Origin_Point = vec;

            Trajectory_Line.material = new Material(Shader.Find("Sprites/Default"));
            Trajectory_Line.material.color = Color.green;

            loopAudio = false;

            cubesColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
            lineColor = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
        }

        void Update()
        {
            int numPoints = Trajectory_Points.Count;
            Vector3[] positions = new Vector3[numPoints];

            for (int i = 0; i < numPoints; i++)
                positions[i] = Trajectory_Points.ElementAt(i).transform.position;

            Trajectory_Line.SetPositions(positions);

            if (Sequencer.playing_mix == true) { playing_in_mix(); };
            
        }

        public void move_TrajPoint(GameObject obj, Vector3 newPos, Quaternion newRot)
        {
            obj.transform.SetPositionAndRotation(newPos, newRot);
        }


        public bool remove_TrajPoint(GameObject obj)
        {
            bool success = false;
            int index = Trajectory_Points.IndexOf(obj);

            // Simply ditch out the point in these Lists
            Raw_Trajectory_Vectors.RemoveAt(index);
            Trajectory_Points.RemoveAt(index);

            // Rebuild Trajectory_Line - because it's not a list
            Trajectory_Line.positionCount = Raw_Trajectory_Vectors.Count;

            for (int i = 0; i < Raw_Trajectory_Vectors.Count; i++)
            {
                Trajectory_Line.SetPosition(i, Raw_Trajectory_Vectors.ElementAt(i));
            }
            return success;
        }

        // IN USE
        public void remove_AllTrajPoints()
        {
            for (int i = 0; i < Trajectory_Points.Count; i++)
            {
                GameObject.Destroy(Trajectory_Points.ElementAt(i).gameObject);
            }

            Raw_Trajectory_Vectors = new List<Vector3>();
            GameObject.Destroy(Trajectory_Line.gameObject);
            GameObject.Destroy(this.gameObject);
        }


        // IN USE
        public void add_TrajPointFromAssetAsChild(Vector3 position, Quaternion quaternion)
        {
            GameObject obj = (GameObject)GameObject.CreatePrimitive(PrimitiveType.Cube);
            
            obj.gameObject.transform.SetPositionAndRotation(position, quaternion);
            obj.gameObject.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            obj.gameObject.tag = "Touchable"; 

            obj.gameObject.AddComponent<HapticProperties>();
            HapticProperties hp = obj.gameObject.GetComponent<HapticProperties>();
            hp.stiffness = 1.0f;
            hp.damping = 0.7f;
            hp.staticFriction = 0.2f;
            hp.dynamicFriction = 0.2f;
            hp.mass = 0.2f;

            obj.GetComponent<Renderer>().enabled = true;
            obj.name = "P" + Raw_Trajectory_Vectors.Count;
            obj.gameObject.GetComponent<Renderer>().material.color = cubesColor;

            obj.transform.SetParent(gameObject.transform);

            Raw_Trajectory_Vectors.Add(position);
            Trajectory_Points.Add(obj);
        }

        public void update_RawData_from_TrajPoints()
        {
            for (int t = 0; t < Trajectory_Points.Count; t++)
            {
                if (Trajectory_Points.ElementAt(t) == null)
                {
                    Debug.Log("Element " +t+ " of Trajectory_Points is non-existent.");
                }
                else { 
                    float x = Trajectory_Points.ElementAt(t).transform.position.x;
                    float y = Trajectory_Points.ElementAt(t).transform.position.y;
                    float z = Trajectory_Points.ElementAt(t).transform.position.z;
                    Raw_Trajectory_Vectors.ElementAt(t).Set(x, y, z);
                }
            }
        }

        // New Method now in use. 
        public void draw_TrajectoryLine_Simplified(float simp)
        {
            int num = Raw_Trajectory_Vectors.Count;
            GameObject.DestroyImmediate(Trajectory_LineGO.GetComponent<LineRenderer>());

            Trajectory_LineGO.AddComponent<LineRenderer>();
            Trajectory_Line = Trajectory_LineGO.GetComponent<LineRenderer>();
            Trajectory_Line.material = new Material(Shader.Find("Sprites/Default"));
            Trajectory_Line.material.color = Color.green;
            Trajectory_Line.startWidth = 0.3f;
            Trajectory_Line.endWidth = 0.3f;
            Trajectory_Line.positionCount = num;

            for (int t = 0; t < Trajectory_Points.Count; t++)
            {
                GameObject.Destroy(Trajectory_Points.ElementAt(t));
            }
            Trajectory_Points = new List<GameObject>();

            int i = 0;
            foreach (Vector3 v in Raw_Trajectory_Vectors)
            {
                Trajectory_Line.SetPosition(i, v);
                i++;
            }

            Trajectory_Line.Simplify(simp);

            Raw_Trajectory_Vectors = new List<Vector3>();

            int numNewVectors = Trajectory_Line.positionCount;

            for (i = 0; i < numNewVectors; i++)
            {
                Vector3 v = Trajectory_Line.GetPosition(i);
                this.add_TrajPointFromAssetAsChild(v, (new Quaternion(0, 0, 0, 0)));
            }
            Trajectory_Line.enabled = true;
            //Trajectory_Line.tag = "Touchable";
            Trajectory_Line.useWorldSpace = false;
        }

        public void update_DistanceData()
        {
            int numVectors = Raw_Trajectory_Vectors.Count;
            float vecDist = 0.0000000f;
            float vecDist_total = 0.0000000f;

            eachDistance = new List<float>();

            for (int i = 0; i < numVectors - 1; i++)
            {
                vecDist = Vector3.Distance(Raw_Trajectory_Vectors.ElementAt(i), Raw_Trajectory_Vectors.ElementAt(i + 1));
                vecDist_total += vecDist;
                eachDistance.Add(vecDist);
            }
            totalLineDistance = vecDist_total;
        }

        public void play()
        {
            playhead.play();
        }


        public void update_Trajectory_Line()
        {
            int numPoints = Trajectory_Points.Count;
            Vector3[] positions = new Vector3[numPoints];

            for (int i = 0; i < numPoints; i++) { 
                positions[i] = Trajectory_Points.ElementAt(i).transform.position;
            }
            Trajectory_Line.SetPositions(positions);
        }

        public void set_TrajectoryLooped(bool b) { Trajectory_Line.loop = b; }

        public void visible_TrajPoints(bool hide)
        {
            if (hide) { 
                foreach (GameObject Traj in Trajectory_Points) 
                {
                    Traj.SetActive(false);
                }
                this.cubesAreHidden = true;
            } else {
                foreach (GameObject Traj in Trajectory_Points)
                {
                    Traj.SetActive(true);
                }
                this.cubesAreHidden = false;
            }

            /////// Testing if this code works
            foreach (GameObject Traj in Trajectory_Points)
            {
                Traj.SetActive(!hide);
            }
            this.cubesAreHidden = hide;
            ////////
        }

        // NOT in use
        public void visible_TrajLine(bool hide) 
        {         
            Trajectory_LineGO.SetActive(!hide);
            this.lineIsHidden = hide;
        }

        public void place_SoundSource_Sphere(float playheadVal)
        {   
            // Following algorithm calculates the playhead value for each trajectory:
            // TrajectoryPlayhead_val = (playhead_mix_sec - t.onset_sec) / t.duration_sec
            // Value below 0.0?  Dont play it.
            // Value between 0.0 and 1.0?  play it!
            // Value hit 1.0 -> stop it!
            // This calculation should go into the trajectories update method. 

            float TrajPlayhead_val = playheadVal;

            // calculate the target position of the sound source sphere...
            float position = TrajPlayhead_val * (Trajectory_Line.positionCount-1);
            float positionBetweenBreakpoints = position - (float)Math.Floor(position);  
            float breakpointNum = position - positionBetweenBreakpoints;                

            if (breakpointNum != Trajectory_Line.positionCount-1) {
                Vector3 p1 = Trajectory_Line.GetPosition((int)breakpointNum);
                Vector3 p2 = Trajectory_Line.GetPosition((int)breakpointNum + 1);

                playhead.SoundSource_Sphere.transform.position = Vector3.Lerp(p1, p2, positionBetweenBreakpoints);
            }
        }

        public void update_colors()
        {
            foreach (GameObject GO in Trajectory_Points)
            {
                GO.gameObject.GetComponent<Renderer>().material.color = cubesColor;
            }
            Trajectory_Line.GetComponent<Renderer>().material.color = lineColor;
            playhead.SoundSource_Sphere.GetComponent<Renderer>().material.color = playheadColor;
        }

        public void hideOrUnhide_Trajectory()
        {
            invisible = !invisible;

            if (invisible)
            {
                Trajectory_LineGO.SetActive(false);
                visible_TrajPoints(false);
            }
            else {
                Trajectory_LineGO.SetActive(true);
                visible_TrajPoints(true);
            }
        }

        public void hide_Trajectory(bool hide)
        {
            invisible = hide;                      
            Trajectory_LineGO.SetActive(!hide);
            visible_TrajPoints(hide);
            playhead.SoundSource_Sphere.SetActive(!hide);
        }


        // This ALSO needs to be removed next session
        // not in use apparently (I searched it via crtl+F)
        public void update_OSC_MSG()
        {
            float x = playhead.SoundSource_Sphere.transform.position.x;
            float y = playhead.SoundSource_Sphere.transform.position.y;
            float z = playhead.SoundSource_Sphere.transform.position.z;
            float playheadTime;
            string path = associatedAudio;

            
            string Traj_ID = "0";
            string adress = "/source/" + Traj_ID;

            OscMessage oscM_audiopath = new SharpOSC.OscMessage(adress, associatedAudio);
            OscMessage oscM_position = new SharpOSC.OscMessage(adress, x, y, z);
            OscMessage oscM_volume = new SharpOSC.OscMessage(adress, volume);

            List<OscMessage> a = new List<OscMessage>();
            a.Add(oscM_audiopath);
            a.Add(oscM_position);
            a.Add(oscM_volume);


            //OscMessage oscM_playingStatus = new SharpOSC.OscMessage(adress, volume);
            // Or trigger play / pause type of message? 

            // double distance = Math.Sqrt( Math.Pow(x,2) + Math.Pow(y,2) + Math.Pow(z,2) );
            // double elevation = Math.Atan(y/x);
            // double azimut = z / distance;

            // MathLib from supercollider does the following: 
            // double distance2 =  Math.Sqrt(Math.Pow(x, 2) + Math.Pow(y, 2) + Math.Pow(z, 2));    // rho
            // double elevation2 = Math.Atan2(y, x);                                               // theta
            // double azimut2 =    Math.Atan2(z, Math.Pow(Math.Pow(x,2) + Math.Pow(y,2), 2) );       // phi

            /* 
        	    rho { ^(x.squared + y.squared + z.squared).sqrt }
	            theta { ^atan2(y, x) }
                phi { ^atan2(z, (x.squared + y.squared).sqrt) }            
            */

        }

        public void sendPositionViaOSC()
        {
            float x = playhead.SoundSource_Sphere.transform.position.x;
            float y = playhead.SoundSource_Sphere.transform.position.y;
            float z = playhead.SoundSource_Sphere.transform.position.z;

            // Workspace size is 10 so I'm dividing by 10 because you can reach it by rotating the view.
            float xNormal = x / 10.0f; // x / 7.61f;  
            float yNormal = y / 10.0f; // y / 8.24f;
            float zNormal = (z / 10.0f)*-1; // z / 7.35f;   // Convert from left hand to right hand. 

			OSCInterface.OSCInterface.set_SourcePosition(this.id, xNormal, yNormal, zNormal);

            // The following equations have worked for the test with ATK BSS. 
			// The coordinates needed to be adjusted and i found out how by experimenting. 
			// I was working out how the coordinates would need to be rotated in order for 
			// the rendered auditory scene to be in line with the visual environment.
			// Later I commented these to use JUST the scaling equations (above) and send the
			// coordinates converted from the left hand system to a right hand system.

            // double rho = Math.Sqrt(Math.Pow(xNormal, 2) + Math.Pow(yNormal, 2) + Math.Pow(zNormal, 2)); // Distance
            // double theta = Math.Acos(zNormal / rho);   // Azimuth  
            // double phi = Math.Atan2(yNormal, xNormal); // Elevation
			// OSCInterface.OSCInterface.set_SourcePosition(this.id, (float)rho, (float)(theta - Math.PI), (float)phi);
        }

        public void hideOrUnhide_Line()
        {
            if (lineColor.a == 200)
            {
                lineColor.a = 0;
            }
            else {
                lineColor.a = 200;
            }
        }

        public void playing_in_mix()
        {
            float localPlayhead_val = (Sequencer.playhead_mix_sec - this.onsetTime) / this.durationTime;
            if (localPlayhead_val >= 0.0f && localPlayhead_val < 1.0f)
            {
                this.place_SoundSource_Sphere(localPlayhead_val);
                this.sendPositionViaOSC();
            }
        }

    }
}