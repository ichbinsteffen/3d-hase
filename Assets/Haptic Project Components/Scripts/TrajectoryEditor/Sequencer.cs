﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SharpOSC;
using OSCInterface;

namespace Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript
{
    class Sequencer : MonoBehaviour
    {
        public float totalPauseDuration_sec;

        #region Members for Single-playing-Sequencer
        TrajectoryScript single_trajectory;
        public bool playing_single;
        public float started_single_sec;
        public float pause_single_sec;
        public float playhead_single_sec;
        public float playhead_single_val;
        public float totalPlayingDuration_single_sec;
        #endregion

        #region Members for Mix-playing-Sequencer
        public static bool playing_mix;
        public static float started_mix_sec;
        public static float pause_mix_sec;
        public static float playhead_mix_sec;
        public static float playhead_mix_val;
        public static float totalPlayingDuration_mix_sec;

        public TrajectoryEditorScript TrajectoryEditor;
        
        public List<float> onsetList;
        public List<float> durationList;
        public List<float> endList;

        private List<GameObject> TrajList;
        private List<Vector3> positionsList;
        private List<OscMessage> oscMessages;
        #endregion


        void Awake()
        {
            onsetList = new List<float>();

            TrajList = TrajectoryEditorScript.Trajectory_List;
            playing_single = false;
            playing_mix = false;
        }

        void Start()
        {
            totalPauseDuration_sec = 0.00000f;
            pause_single_sec = 0.0000f;
            pause_mix_sec = 0.0000f;
        }   


    void Update()
        {
            if (playing_single)
            {
                playhead_single_sec = Time.time - started_single_sec; //  - totalPauseDuration_sec; // + PAUSEDURATION // plus or minus?!?!?!?
                playhead_single_val = playhead_single_sec / totalPlayingDuration_single_sec;
                
                single_trajectory.sendPositionViaOSC();

                // How else can I trigger pause when the playhead reaches 1.0 ? 
                if (playhead_single_sec == totalPlayingDuration_single_sec || playhead_single_sec >= totalPlayingDuration_single_sec)
                {
                    pause_single();
                }
            }

            if (playing_mix)
            {
                playhead_mix_sec = Time.time - started_mix_sec;
                playhead_mix_val = playhead_mix_sec / totalPlayingDuration_mix_sec;

                // Following algorithm calculates the playhead value for each trajectory:
                // TrajectoryPlayhead_val = (playhead_mix_sec - t.onset_sec) / t.duration_sec
                // Value below 0.0?  Dont play it.
                // Value between 0.0 and 1.0?  play it!
                // Value hit 1.0 -> stop it!
                // This calculation should go into the trajectories update method. 

                if (playhead_mix_sec == totalPlayingDuration_mix_sec || playhead_mix_sec >= totalPlayingDuration_mix_sec)
                {
                    pause_mix();
                }
            }

            else {
                //tempPauseDuration_sec = Time.time - pause_sec;
                //totalPauseDuration_sec += tempPauseDuration_sec;
                totalPauseDuration_sec += Time.deltaTime;
            }
        }

        public void start_playing_mix_from(float sliderValue)
        {
            update_TotalPlayingDuration();
            started_mix_sec = Time.time - (sliderValue * totalPlayingDuration_mix_sec);
            playing_mix = true;

            int count = TrajList.Count;
            onsetList = new List<float>(count);
            durationList = new List<float>(count);
            endList = new List<float>(count);

            for (int i = 0; i < count; i++)
            {
                TrajectoryScript t = TrajList.ElementAt(i).GetComponent<TrajectoryScript>();
                onsetList.Add(0.0f);
                durationList.Add(0.0f);
                endList.Add(0.0f);

                onsetList[i] = t.onsetTime + started_mix_sec;
                durationList[i] = t.durationTime;
                endList[i] = onsetList[i] + durationList[i];
            }
        }

        public void pause_mix()
        {
            playing_mix = false;
            pause_mix_sec = Time.time;
        }

        public void pause_single()
        {
            playing_single = false;
            pause_single_sec = Time.time;
        }


        // I Dont think this this method is needed
        
        public void update_onsetAndDurationLists()
        {
            int count = TrajList.Count;
            onsetList = new List<float>(count);
            durationList = new List<float>(count);
            endList = new List<float>(count);

            for (int i = 0; i < count; i++)
            {
                TrajectoryScript t = TrajList.ElementAt(i).GetComponent<TrajectoryScript>();
                onsetList.Add(0.0f);
                durationList.Add(0.0f);
                endList.Add(0.0f);

                onsetList[i] = t.onsetTime + started_mix_sec;
                durationList[i] = t.durationTime;
                endList[i] = onsetList[i] + durationList[i];

                totalPlayingDuration_mix_sec = endList.Max() - started_mix_sec;
            }
        }
        

        public void update_TotalPlayingDuration()
        {
            int count = TrajList.Count;
            List<float> endTimesListWithoutOffset = new List<float>(count);

            for (int i = 0; i < count; i++)
            {
                TrajectoryScript t = TrajList.ElementAt(i).GetComponent<TrajectoryScript>();    
                endTimesListWithoutOffset.Add(0.0f);
                endTimesListWithoutOffset[i] = t.onsetTime + t.durationTime;
            }
            totalPlayingDuration_mix_sec = endTimesListWithoutOffset.Max();
        }

        // When the Slider-Handle is being dragged to somewhere on the Slider, the playheadTime needs to adjust.
        public void set_playtimeFromSliderVal(float sliderValue)
        {
            pause_single();
            //playheadTime_sec = sliderValue * totalPlayingDuration_sec;
            // No I think this is the right approach, since it doesnt interfere with the algorithm in the Update method:
            started_single_sec = sliderValue * totalPlayingDuration_single_sec;
        }

        public void start_playing_single_from(float sliderValue, TrajectoryScript t)
        {
            single_trajectory = t;
            started_single_sec = Time.time - (sliderValue * totalPlayingDuration_single_sec);
            playing_single = true;
        }

        public void update_SinglePlayingDuration(float duration)
        {
            totalPlayingDuration_single_sec = duration;
        }
    }
}
