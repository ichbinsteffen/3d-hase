﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript;
using System.Runtime.InteropServices;
using System.Drawing;


namespace Assets.Haptic_Project_Components.Scripts.TrajectoryEditorScript.HUDSpace
{
    class theGUI : MonoBehaviour
    {
        public TrajectoryEditorScript trajectoryEditorScript;

        private List<GameObject> Traj_List;
        private TrajectoryScript currentEditTraj;


        // Mode-Independent Components

        public Dropdown EditorModeDropdown; // Dropdown that is always visible
        public Sequencer seq;


        // Editor-Mode Components
        public Canvas EditModeCanvas;
        public Dropdown singleTrajDropdown;
        private List<Dropdown.OptionData> SingleSelectDropdownOptions;

        public InputField OnsetTimeField;
        public InputField DurationTimeField;
        public Button SelectAudioButton;
        public Text SelectedAudioText;
        public Button HideCubesButton;
        public Button LoopLineButton;
        public Button CubesColorButton;
        public Button LineColorButton;
        public Slider LineWidthSlider;
        public Button PlayheadColorButton;
        public Button ApplyButton;
        private System.Windows.Forms.ColorDialog windowsColorDialog = new System.Windows.Forms.ColorDialog();
        public Button LoopAudioButton;
        public Slider VolumeSlider;
        public Text VolumeText;

        public UISelectHandler selectHandler;
        private bool isScrubberSelectedPrev = false;

        public Slider localSlider;
        public Button localPlayPauseButton;
        public Text localPlayheadTimeText;


        // Draw-Mode Components
        public Canvas DrawModeCanvas;
        public Occult.UI.MultiSelectDropdown multiTrajDropdown;
        private List<Occult.UI.MultiSelectDropdown.OptionData> MultiSelectDropdownOptions;
        public Button AddTrajButton;

        public Slider SimplificationSlider;
        public Button SimplificationButton;
        public Text SimplifyValueText;


        // Play-Mode Components
        public Canvas PlayModeCanvas;
        public Slider globalSlider;
        public Button globalPlayPauseButton;
        public Text globalPlayheadTimeText;

        // options canvas Components
        public Canvas OptionsCanvas;
        public InputField oscRecIP;
        public InputField oscRecPort;

        void Start()
        {
            EditorModeDropdown.onValueChanged.AddListener(delegate { changeEditorMode(); });

            SingleSelectDropdownOptions = new List<Dropdown.OptionData>();
            MultiSelectDropdownOptions = new List<Occult.UI.MultiSelectDropdown.OptionData>();

            singleTrajDropdown.onValueChanged.AddListener(delegate { SetSingleTrajVisibility(); update_HideCubesButtonState(); });
            multiTrajDropdown.onValueChanged.AddListener(delegate { SetMultiTrajVisibility(); });

            AddTrajButton.onClick.AddListener(delegate { AddTrajButtonClicked(); });

            SelectAudioButton.onClick.AddListener(delegate { OpenFileBrowser(); });
            //SelectedAudioText.gameObject.SetActive(false);

            EditModeCanvas.gameObject.SetActive(false);
            PlayModeCanvas.gameObject.SetActive(false);

            SimplificationSlider.onValueChanged.AddListener(delegate { SimplificationSliderChanged(); });
            SimplificationButton.onClick.AddListener(delegate { SimplificationButtonClicked(); });

            HideCubesButton.GetComponentInChildren<Text>().text = "";
            LoopLineButton.GetComponentInChildren<Text>().text = "";
            HideCubesButton.onClick.AddListener(delegate { hide_unhide_cubes(); });
            LoopLineButton.onClick.AddListener(delegate { loop_unloop_Line(); });
            CubesColorButton.onClick.AddListener(delegate { open_CubeColorPicker(); });
            LineColorButton.onClick.AddListener(delegate { open_LineColorPicker(); });
            PlayheadColorButton.onClick.AddListener(delegate { open_PlayheadColorPicker(); });
            ApplyButton.onClick.AddListener(delegate { set_onset_and_duration(); });
            LoopAudioButton.onClick.AddListener(delegate { loop_unloop_Audio(); });
            VolumeSlider.onValueChanged.AddListener(delegate { set_Volume(); });
            LineWidthSlider.onValueChanged.AddListener(delegate { set_LineWidth(); });

            localSlider.onValueChanged.AddListener(delegate { update_SoundSource_Sphere_single_play(); });
            globalSlider.onValueChanged.AddListener(delegate { update_all_playheads();  });

            selectHandler = localSlider.gameObject.GetComponent<UISelectHandler>();

            globalPlayPauseButton.onClick.AddListener(delegate { playAndPauseGlobal(); });
            localPlayPauseButton.onClick.AddListener(delegate { playAndPauseLocal(); });

            Traj_List = TrajectoryEditorScript.Trajectory_List;



            //selectHandler.IsSelected 

            seq = gameObject.GetComponent<Sequencer>();
        }

        void Update()
        {
            if (multiTrajDropdown.gameObject.activeSelf == true) { this.UpdateMultiSelectDropdownOptions(); }
            if (singleTrajDropdown.gameObject.activeSelf == true) { this.UpdateSingleSelectDropdownOptions(); }

            if (singleTrajDropdown.options.Count >= 1)
                currentEditTraj = Traj_List.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            //bool isScrubberSelectedPrev = false;
            if (!selectHandler.IsSelected)
            {
                if (isScrubberSelectedPrev)
                {
                    // Edge event, user has just released the mouse button.
                    // Set the timeline value to match the slider position.
                    // timelineValue = sliderScrubber.value;
                    Slider s = localSlider;
                    if (EditorModeDropdown.value == 3)
                    {
                        s = localSlider;
                        seq.start_playing_single_from(s.value, Traj_List.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>());
                    }
                    if (EditorModeDropdown.value == 4)
                    {
                        s = globalSlider;
                        seq.start_playing_mix_from(s.value);
                    }

                    Debug.Log("Playing now from" + s.value);
                }
                else
                {
                    // Not selected, slider follows the timeline.

                    ///localSlider.value = timelineValue;

                }
            }
            isScrubberSelectedPrev = selectHandler.IsSelected;



            switch (EditorModeDropdown.value)
            {
                case 0:
                    break;

                case 1:
                    break;

                case 2:
                    float sec1 = seq.playhead_single_sec;
                    if (seq.playing_single) localSlider.value = seq.playhead_single_val;
                    else { localPlayPauseButton.GetComponentInChildren<Text>().text = "►"; }
                    localPlayheadTimeText.text = (seq.playhead_single_sec).ToString();
                    string minutes = ((int)sec1 / 60).ToString();
                    string seconds = (sec1 % 60).ToString("f2");
                    localPlayheadTimeText.text = minutes + ":" + seconds;
                    selectHandler = localSlider.gameObject.GetComponent<UISelectHandler>();
                    break;

                case 3:
                    float sec2 = Sequencer.playhead_mix_sec;
                    if (Sequencer.playing_mix) globalSlider.value = Sequencer.playhead_mix_val;
                    else { globalPlayPauseButton.GetComponentInChildren<Text>().text = "►"; }
                    string minutes2 = ((int)sec2 / 60).ToString();
                    string seconds2 = (sec2 % 60).ToString("f2");
                    globalPlayheadTimeText.text = minutes2 + ":" + seconds2;
                    selectHandler = globalSlider.gameObject.GetComponent<UISelectHandler>();
                    break;
                    
            }

        }


        public void AddTrajButtonClicked()
        {
            TrajectoryEditorScript.add_Trajectory();
        }

        public void SimplificationSliderChanged()
        {
            SimplifyValueText.text = string.Format("{0:N3}", SimplificationSlider.value);
        }

        public void SimplificationButtonClicked()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();
            
            traj.draw_TrajectoryLine_Simplified(SimplificationSlider.value);
        }

        public void changeEditorMode()
        {
            switch (EditorModeDropdown.value)
            {
                case 0:
                    TrajectoryEditorScript.editorMode = EditorMode.lookAndFeel;

                    multiTrajDropdown.gameObject.SetActive(true);

                    DrawModeCanvas.gameObject.SetActive(false);
                    EditModeCanvas.gameObject.SetActive(false);
                    PlayModeCanvas.gameObject.SetActive(false);
                    OptionsCanvas.gameObject.SetActive(false);

                    SetMultiTrajVisibility();

                    force_PauseGlobalPlayer();
                    force_PauseLocalPlayer();
                    break;

                case 1:
                    TrajectoryEditorScript.editorMode = EditorMode.draw;

                    multiTrajDropdown.gameObject.SetActive(true);
                    singleTrajDropdown.gameObject.SetActive(true);

                    DrawModeCanvas.gameObject.SetActive(true);
                    EditModeCanvas.gameObject.SetActive(false);
                    PlayModeCanvas.gameObject.SetActive(false);
                    OptionsCanvas.gameObject.SetActive(false);

                    SetMultiTrajVisibility();
                    force_PauseGlobalPlayer();
                    force_PauseLocalPlayer();
                    break;

                case 2:
                    TrajectoryEditorScript.editorMode = EditorMode.edit;

                    multiTrajDropdown.gameObject.SetActive(false);
                    singleTrajDropdown.gameObject.SetActive(true);

                    DrawModeCanvas.gameObject.SetActive(false);
                    PlayModeCanvas.gameObject.SetActive(false);
                    EditModeCanvas.gameObject.SetActive(true); SelectedAudioText.gameObject.SetActive(true);
                    OptionsCanvas.gameObject.SetActive(false);

                    SetSingleTrajVisibility();
                    force_PauseGlobalPlayer();
                    break;

                case 3:
                    TrajectoryEditorScript.editorMode = EditorMode.playCue;

                    multiTrajDropdown.gameObject.SetActive(true);
                    singleTrajDropdown.gameObject.SetActive(false);

                    DrawModeCanvas.gameObject.SetActive(false);
                    PlayModeCanvas.gameObject.SetActive(true);
                    EditModeCanvas.gameObject.SetActive(false);
                    OptionsCanvas.gameObject.SetActive(false);

                    seq.update_onsetAndDurationLists();
                    SetMultiTrajVisibility();
                    force_PauseGlobalPlayer();
                    force_PauseLocalPlayer();

                    hide_unhide_ALL_TrajectoryLines();
                    hide_unhide_ALL_Cubes();

                    break;

                case 4:

                    multiTrajDropdown.gameObject.SetActive(false);
                    singleTrajDropdown.gameObject.SetActive(false);

                    DrawModeCanvas.gameObject.SetActive(false);
                    PlayModeCanvas.gameObject.SetActive(false);
                    EditModeCanvas.gameObject.SetActive(false);

                    OptionsCanvas.gameObject.SetActive(true);
                    break;

            }
        }

        private void SetMultiTrajVisibility()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int numTrajectories = goList.Count;

            int amountSelected = multiTrajDropdown.value.Count;

            for (int j = 0; j < numTrajectories; j++)
            {
                //goList.ElementAt(j).gameObject.SetActive(false);
                goList.ElementAt(j).GetComponent<TrajectoryScript>().hide_Trajectory(true);

            }
            if (amountSelected >= 0) {
                for (int i = 0; i < amountSelected; i++)
                {
                    int indexOfActivatedTraj = multiTrajDropdown.value.ElementAt(i);
                    //goList.ElementAt(indexOfActivatedTraj-1).gameObject.SetActive(true);
                    goList.ElementAt(indexOfActivatedTraj - 1).GetComponent<TrajectoryScript>().hide_Trajectory(false);
                }
            }
            else { goList.ElementAt(0).GetComponent<TrajectoryScript>().hide_Trajectory(false); }

            trajectoryEditorScript.update_haptics();
        }

        private void SetSingleTrajVisibility()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int numTrajectories = goList.Count;

            for (int j = 0; j < numTrajectories; j++)
            {
                goList.ElementAt(j).GetComponent<TrajectoryScript>().hide_Trajectory(true);
            }

            TrajectoryScript t = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            if (singleTrajDropdown.options.Count >= 1)
            {
                //goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>().hide_Trajectory(false);
                t.hide_Trajectory(false);
                // This line puts the current one in Single-Dropdown as active in the Multi-Dropdown. This brings up interference.
                // multiTrajDropdown.value.Add(singleTrajDropdown.value+1);
            }

            trajectoryEditorScript.update_haptics();

            Update_SelectedAudioText();
            update_HideCubesButtonState();
            update_LoopLineButtonState();
            update_loopAudioButtonState();
            update_TimeFields();
            update_Volume(t.volume);

            seq.update_SinglePlayingDuration(t.durationTime);
        }



        public void UpdateHUD()
        {
            int modeID = (int)TrajectoryEditorScript.editorMode;
            int currentTrajCount = TrajectoryEditorScript.trajCount;
        }

        public void UpdateSingleSelectDropdownOptions()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int numTrajectories = goList.Count;
            SingleSelectDropdownOptions.Clear();

            for (int i = 0; i < numTrajectories; i++)
            {
                SingleSelectDropdownOptions.Add(new Dropdown.OptionData());
                SingleSelectDropdownOptions.ElementAt(i).text = goList.ElementAt(i).gameObject.name;
            }
            singleTrajDropdown.options = SingleSelectDropdownOptions;
        }

        public void UpdateMultiSelectDropdownOptions()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int numTrajectories = goList.Count;
            MultiSelectDropdownOptions.Clear();

            for (int i = 0; i < numTrajectories; i++)
            {
                MultiSelectDropdownOptions.Add(new Occult.UI.MultiSelectDropdown.OptionData());
                MultiSelectDropdownOptions.ElementAt(i).text = goList.ElementAt(i).gameObject.name;
            }
            multiTrajDropdown.options = MultiSelectDropdownOptions;
        }

        public void OpenFileBrowser()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();

            ofd.Filter = "WAV files (*.wav)|*.wav";
            ofd.FilterIndex = 2;
            ofd.RestoreDirectory = true;
            ofd.Multiselect = false;
            ofd.Title = "Open a wav soundfile to associate with the Trajectory";

            string userName = System.Environment.UserName;
            string sourceFile;
            string sourceFolder = "";

            string audioSource_path = "C:\\Users\\" + userName + "\\Music\\TrajectoryEditor_Audio";

            if (!System.IO.Directory.Exists(audioSource_path)) System.IO.Directory.CreateDirectory(audioSource_path);

            ofd.InitialDirectory = audioSource_path;

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                sourceFile = ofd.FileName;
                sourceFolder = sourceFile;
                
                traj.associatedAudio = sourceFile;

                int trajID = singleTrajDropdown.value;
                OSCInterface.OSCInterface.set_AudioFilePath(trajID, sourceFile);
            }
            this.Update_SelectedAudioText();
        }

        private void open_CubeColorPicker()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            windowsColorDialog.ShowDialog();
            windowsColorDialog.FullOpen = true;

            System.Drawing.Color userColor = windowsColorDialog.Color;
            UnityEngine.Color32 unityColor;

            unityColor.r = userColor.R;
            unityColor.g = userColor.G;
            unityColor.b = userColor.B;
            unityColor.a = userColor.A;

            traj.cubesColor = unityColor;
            traj.update_colors();

            //currentEditTraj.cubesColor = unityColor;
            //currentEditTraj.update_colors();
        }

        private void open_LineColorPicker()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            windowsColorDialog.ShowDialog();
            windowsColorDialog.FullOpen = true;

            System.Drawing.Color userColor = windowsColorDialog.Color;
            UnityEngine.Color32 unityColor;

            unityColor.r = userColor.R;
            unityColor.g = userColor.G;
            unityColor.b = userColor.B;
            unityColor.a = userColor.A;

            traj.lineColor = unityColor;
            traj.update_colors();
        }

        private void open_PlayheadColorPicker()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            windowsColorDialog.ShowDialog();
            windowsColorDialog.FullOpen = true;

            System.Drawing.Color userColor = windowsColorDialog.Color;
            UnityEngine.Color32 unityColor;

            unityColor.r = userColor.R;
            unityColor.g = userColor.G;
            unityColor.b = userColor.B;
            unityColor.a = userColor.A;

            traj.playheadColor = unityColor;
            traj.update_colors();
        }

        public void Update_SelectedAudioText()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            if (goList.Count != 0)
            {
                SelectedAudioText.text = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>().associatedAudio;               
                SelectedAudioText.text = Path.GetFileName(SelectedAudioText.text);
            }
        }

        private void update_HideCubesButtonState()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            if (traj.cubesAreHidden) { HideCubesButton.GetComponentInChildren<Text>().text = "•"; }
            else { HideCubesButton.GetComponentInChildren<Text>().text = ""; }

            //if (currentEditTraj.cubesAreHidden) { HideCubesButton.GetComponentInChildren<Text>().text = "•"; }
            //else { HideCubesButton.GetComponentInChildren<Text>().text = ""; }
        }

        private void update_LoopLineButtonState()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();
            LineRenderer traj_Line = traj.Trajectory_LineGO.GetComponent<LineRenderer>();

            //LineRenderer traj_Line = currentEditTraj.GetComponent<LineRenderer>();

            if (traj_Line.loop) { LoopLineButton.GetComponentInChildren<Text>().text = "•"; }
            else { LoopLineButton.GetComponentInChildren<Text>().text = ""; }
        }

        private void update_TimeFields()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            OnsetTimeField.text = Convert.ToString(traj.onsetTime);
            DurationTimeField.text = Convert.ToString(traj.durationTime);

            //OnsetTimeField.text = Convert.ToString(currentEditTraj.onsetTime);
            //DurationTimeField.text = Convert.ToString(currentEditTraj.durationTime);
        }

        private void set_onset_and_duration()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript t = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            t.onsetTime = float.Parse(OnsetTimeField.textComponent.text);
            t.durationTime = float.Parse(DurationTimeField.textComponent.text);

            seq.update_SinglePlayingDuration(t.durationTime);
        }

        private void hide_unhide_cubes()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            if (traj.cubesAreHidden == true) traj.visible_TrajPoints(false);
            else traj.visible_TrajPoints(true);

            //if (currentEditTraj.cubesAreHidden == true) currentEditTraj.visible_TrajPoints(false);
            //else currentEditTraj.visible_TrajPoints(true);

            update_HideCubesButtonState();
        }

        private void loop_unloop_Line()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int index = singleTrajDropdown.value;
            LineRenderer traj_Line = goList.ElementAt(index).GetComponent<LineRenderer>();

            traj_Line.loop = !traj_Line.loop;
            update_LoopLineButtonState();
        }

        private void loop_unloop_Audio()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();
            traj.loopAudio = !traj.loopAudio;

            update_loopAudioButtonState();
        }

        private void update_loopAudioButtonState()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            if (traj.loopAudio == true) { LoopAudioButton.GetComponentInChildren<Text>().text = "•"; }
            else { LoopAudioButton.GetComponentInChildren<Text>().text = ""; }
        }

        private void onSliderDrag()
        {
            //globalSlider.OnInitializePotentialDrag        
        }

        private void playAndPauseGlobal()
        {
            if (Sequencer.playing_mix)
            {
                seq.pause_mix();
                globalPlayPauseButton.GetComponentInChildren<Text>().text = "►";
            }
            else
            {
                //seq.start_playing();
                seq.start_playing_mix_from(globalSlider.value);
                globalPlayPauseButton.GetComponentInChildren<Text>().text = "▌▌";
            }
        }

        private void playAndPauseLocal()
        {
            if (seq.playing_single)
            {
                seq.pause_single();
                localPlayPauseButton.GetComponentInChildren<Text>().text = "►";

                List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
                TrajectoryScript t = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();
                OSCInterface.OSCInterface.pause_playing(t.id);
            }
            else
            {
                List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
                TrajectoryScript t = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

                //seq.update_SinglePlayingDuration(traj);
                if (t.durationTime < 0)
                {
                    System.Windows.Forms.Form msgBox = new System.Windows.Forms.Form();
                    msgBox.Text = "You forgot to enter a duration for the Trajectory!";
                    //msgBox.AcceptButton
                    //msgBox.AcceptButton = new System.Windows.Forms.Message("hello");
                    msgBox.ShowDialog();
                }
                else
                {
                    seq.start_playing_single_from(localSlider.value, t);
                    localPlayPauseButton.GetComponentInChildren<Text>().text = "▌▌";

                    OSCInterface.OSCInterface.start_playing(t.id, t.onsetTime, t.loopAudio);
                }
            }
        }

        private void force_PauseGlobalPlayer()
        {
            if (seq.playing_single)
            {
                seq.pause_single();
                globalPlayPauseButton.GetComponentInChildren<Text>().text = "►";
            }
        }

        private void force_PauseLocalPlayer()
        {
            if (seq.playing_single)
            {
                seq.pause_single();
                localPlayPauseButton.GetComponentInChildren<Text>().text = "►";
            }
        }

        private void set_Volume()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            traj.volume = VolumeSlider.value;
            VolumeText.text = string.Format("{0:N2}", VolumeSlider.value);
            //VolumeText.text = VolumeSlider.value.ToString();
        }
        private void update_Volume(float vol)
        {
            VolumeSlider.value = vol;
            VolumeText.text = string.Format("{0:N2}", vol);//vol.ToString();

            // string.Format("{0:N3}", SimplificationSlider.value);
        }

        public void update_SoundSource_Sphere_single_play()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript t = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();

            t.place_SoundSource_Sphere(localSlider.value);
        }

        public void update_all_playheads()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            int amountSelected = multiTrajDropdown.value.Count;

            if (amountSelected > 0)
            {
                for (int i = 0; i < amountSelected; i++)
                {
                    int indexOfActivatedTraj = multiTrajDropdown.value.ElementAt(i);
                    
                    //goList.ElementAt(indexOfActivatedTraj - 1).GetComponent<TrajectoryScript>().place_SoundSource_Sphere();
                }
            }
            
            // t.place_SoundSource_Sphere(globalSlider.value);
        }

        private void hide_unhide_ALL_TrajectoryLines()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;

            foreach (GameObject t in goList)
            {
                TrajectoryScript ts = t.GetComponent<TrajectoryScript>();
                ts.hideOrUnhide_Line();
            }
        }

        private void hide_unhide_ALL_Cubes()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;

            foreach (GameObject t in goList)
            {
                TrajectoryScript ts = t.GetComponent<TrajectoryScript>();
                ts.visible_TrajPoints(true);
            }
        }

        public void set_LineWidth()
        {
            List<GameObject> goList = TrajectoryEditorScript.Trajectory_List;
            TrajectoryScript traj = goList.ElementAt(singleTrajDropdown.value).GetComponent<TrajectoryScript>();
            LineRenderer traj_Line = traj.Trajectory_LineGO.GetComponent<LineRenderer>();

            traj_Line.SetWidth(LineWidthSlider.value, LineWidthSlider.value);

        }


    }
}
